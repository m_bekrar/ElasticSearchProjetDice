
# -*- coding: utf-8 -*-

import os
from elasticsearch import Elasticsearch
es = Elasticsearch()

###### A changer pour chaque repertoire ######

INDEX_NAME = 'oseo'

TYPE_NAME = '2012'

dir = '/Users/mbekrar/Desktop/oseo/2012'

#### paramettre de l'index ####

indexe = {
    "settings": {
        "index": {
            "analysis": {
                "analyzer": {
                    "default": {
                        "type": "custom",
                        "tokenizer": "uax_url_email",
                        "filter": ["standard", "lowercase", "stop"]
                    }
                }
            }
        }
    }
}

######### changer attachment  ########

map = {

        "2012": {
            "_source": {
                "excludes": [
                    "file"
                ]
            },
            "properties": {
                "file": {
                    "type": "attachment",
                    "index": "yes",
                    "path": "full",
                    "fields": {
                        "title": {"store": "yes"},
                        "file": {"term_vector": "with_positions_offsets", "type": "string", "store": "yes"}
                    }
                }
            }
        }
    }

try:
    es.indices.delete(index=INDEX_NAME, ignore=[400, 404])
except Exception, e:
    print e


try:
    es.indices.create(index=INDEX_NAME , body = indexe, ignore=400)
except Exception, e:
    print e


try:
    es.indices.put_mapping( index=INDEX_NAME, doc_type=TYPE_NAME, body= map)
except Exception, e:
    print e


##### Encoder base64 ########


i = 1
for path, dirs, files in os.walk(dir):

        for file in files:

            fname = os.path.join(path,file)

            if fname.endswith('.pdf'):

                print fname

                file64 = open(fname, "rb").read().encode("base64")
                sample = {}
                sample['file'] = file64
                sample['title'] = fname

                try:
                    res = es.index(index=INDEX_NAME, doc_type=TYPE_NAME, id=i, body = sample)

                except Exception, e:
                    print e

                del sample
                i += 1

ress = es.indices.stats(index=INDEX_NAME)['_all']['total']['docs']['count']

print ress

