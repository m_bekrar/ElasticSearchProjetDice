# -*- coding: utf-8 -*-
from numpy import zeros
from scipy.linalg import svd
#following needed for TFIDF
from math import log
from numpy import asarray, sum
import re

titles = ["La révolution mobile continue de jour en jour à changer la façon dont l'industrie fonctionne",
          "En marge de ce changement important, une industrie semble rester en retrait dans ce domaine : la santé",
          "Pourtant, il est évident que la santé pourrait bénéficier de la puissance des technologies actuelles, du smartphone aux systèmes sécurisés de partage de données"
        ]
StopWords = []
f = open("SmartStoplist.txt", 'r')
content = f.readlines()

for line in content:
    matchToken = re.search('(.*)', line)
    if matchToken:
        stop = matchToken.group().decode("UTF-8")
        StopWords.append(stop)
#stopwords = ['and','edition','for','in','little','of','the','to']

stopwords = []
for stop in StopWords:

    stop = stop.replace('\r', '')
    stopwords.append(stop)

print stopwords
#[str(x) for x in stopwords]
#print stopwords
ignorechars = ''',:'!'''

class LSA(object):
    def __init__(self, stopwords, ignorechars):
        self.stopwords = stopwords
        self.ignorechars = ignorechars
        self.wdict = {}
        self.dcount = 0        
    def parse(self, doc):
        words = doc.split();
        print words
        for w in words:
            w = w.lower().translate(None, self.ignorechars)
            if w in self.stopwords:
                print "stop word"
                print w
                continue
            elif w in self.wdict:
                self.wdict[w].append(self.dcount)
                print "mot deja existant dans dict"
                print w
                print self.dcount
            else:
                self.wdict[w] = [self.dcount]
                print "nouveau mot du dict"
                print w
                print self.dcount

        self.dcount += 1
    def build(self):
        print "wdict:", self.wdict
        self.keys = [k for k in self.wdict.keys() if len(self.wdict[k]) > 1]
        self.keys.sort()
        self.A = zeros([len(self.keys), self.dcount])
        print "Keys: ",self.keys

        for i, k in enumerate(self.keys):
            for d in self.wdict[k]:
                self.A[i,d] += 1
        print 'A :', self.A
    def calc(self):
        self.U, self.S, self.Vt = svd(self.A)
    def TFIDF(self):
        WordsPerDoc = sum(self.A, axis=0)        
        DocsPerWord = sum(asarray(self.A > 0, 'i'), axis=1)
        rows, cols = self.A.shape
        for i in range(rows):
            for j in range(cols):
                self.A[i,j] = (self.A[i,j] / WordsPerDoc[j]) * log(float(cols) / DocsPerWord[i])
    def printA(self):
        print 'Here is the count matrix'
        print self.A
    def printSVD(self):
        print 'Here are the singular values'
        print self.S
        print 'Here are the first 3 columns of the U matrix'
        print -1*self.U[:, 0:3]
        print 'Here are the first 3 rows of the Vt matrix'
        print -1*self.Vt[0:3, :]

mylsa = LSA(stopwords, ignorechars)
for t in titles:
    mylsa.parse(t)
mylsa.build()
mylsa.printA()
mylsa.calc()
mylsa.printSVD()
