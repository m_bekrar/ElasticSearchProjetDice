# -*- coding: utf-8 -*-
__author__ = 'mbekrar'
import requests
import io
import re
import json

doc=[]
StopWords = []
f = open("SmartStoplist.txt", 'r')
content = f.readlines()
###### Consruire stopListe
for line in content:
    matchToken = re.search('(.*)', line)
    if matchToken:
        stop = matchToken.group().decode("UTF-8")
        StopWords.append(stop)


########### Récupération de la liste des tokens ainsi que leurs fréquences
from elasticsearch import Elasticsearch
ess = Elasticsearch()

ress = ess.termvector(index='test', doc_type='attachment', id="2",field_statistics=False, fields='file', offsets=False, positions=False) #AUy9rugKHq7548eTI7tF

liste = ress['term_vectors']['file']['terms']
mots = []
frequence = []
####Toute la liste sans nettoyer
for token in liste:
    mots.append(token)
    frequence.append(liste[token]['term_freq'] )

#####Organiser la lsite des stopWords
Stop = []
for stop in StopWords:

    stop = stop.replace('\r', '')
    Stop.append(stop)

########### Enlever les stopWords de la liste des "mots"
newListe = []

for mot in mots:
    if not(mot in Stop):
        newListe.append(mot)
######## Enlever les nombres
lastListe = []
for mot in newListe:
    matchNumber = re.search(r'^[0-9]+',mot)
    if not matchNumber:
        lastListe.append(mot)
#########associer à chaque mot sa fréquence
mot_freq =[]
for key in lastListe:
    mot_freq.append(key)
    mot_freq.append(liste[key]['term_freq'])

############# enlever les apostrophes
j=0
Table =[]
Sable =[]
while j<len(mot_freq) :

    matchChar=re.search(r"^[a-zA-Z][']", mot_freq[j])
    if matchChar:

        mot = mot_freq[j].replace(matchChar.group(),'')
    else:
        matchCode=re.search(r"[']", mot_freq[j])
        if matchCode:
            mot = mot_freq[j].replace(matchCode.group(),' ')
        else:
            regex = re.search(ur"^([a-zA-Z]’|[a-zA-Z]‘)", mot_freq[j], re.UNICODE)
            if regex :
                mot = mot_freq[j].replace(regex.group(),'')
            else:
                lastTry = re.search(ur"(’|‘)", mot_freq[j], re.UNICODE)
                if lastTry:
                    mot = mot_freq[j].replace(lastTry.group(),' ')
                else:
                    mot = mot_freq[j]
    Table.append(mot)
    Sable.append(mot_freq[j+1])

    j += 2

####### Enlever de nouveau les stopwords après l'enlèvement des apostrophes
laPlusPropre =[]
laPlusPropreAvecFreq=[]
for i in range(0,len(Table)):
    if not (Table[i] in Stop):
        laPlusPropre.append(Table[i])
        laPlusPropreAvecFreq.append(Table[i])
        laPlusPropreAvecFreq.append(Sable[i])

######## LSA, TF-IDF m prepare le document
j=0
document = ''
while (j < len(laPlusPropreAvecFreq)):
    for i in range (0, laPlusPropreAvecFreq[j+1]):
        document = document+' '+ laPlusPropreAvecFreq[j]
    j+=2

from gensim import corpora, models, similarities

documents = []


import re
StopWords = ''
f = open("SmartStoplist.txt", 'r')
content = f.readlines()
f.close()
for line in content:
    matchToken = re.search('(.*)', line)
    if matchToken:
        stop  = matchToken.group().decode("UTF-8")
        stop = stop.replace('\r', '')
        StopWords = StopWords+' '+stop
# remove common words and tokenize
documents.append(document)
documents.append('pour avoir la ')

f = open('mycorpus.txt','w')
document = document.encode("UTF-8")
f.write(document)
f.close()

stoplist = set(StopWords.split())

texts = [[word for word in document.lower().split() if word not in stoplist]
          for document in documents]
#print texts

# remove words that appear only once
from collections import defaultdict
frequency = defaultdict(int)
for text in texts:
    for token in text:
        frequency[token] += 1
texts = [[token for token in text if frequency[token] > 1]
          for text in texts]
from pprint import pprint   # pretty-printer
pprint(texts)

dictionary = corpora.Dictionary(texts)
dictionary.save('/tmp/deerwester.dict') # store the dictionary, for future reference
#print(dictionary)
#print(dictionary.token2id)

corpus = [dictionary.doc2bow(text) for text in texts]
#
#print corpus

corpora.MmCorpus.serialize('/tmp/deerwester.mm', corpus)

print '************************** Corpus *****************************'
print(corpus)
import gensim
from gensim import corpora, models, similarities
from gensim import *
dictionary = corpora.Dictionary.load('/tmp/deerwester.dict')
corpus = corpora.MmCorpus('/tmp/deerwester.mm')
print(corpus)

tfidf = models.TfidfModel(corpus)

print 'TFIDF    :', tfidf

corpus_tfidf = tfidf[corpus]

for doc in corpus_tfidf:
    print '************** Doc ****************'
    print(doc)

#print '********************** corpus tfidf ******************'
#print corpus_tfidf[0]

lsi = models.LsiModel(corpus_tfidf, id2word=dictionary, num_topics=2) # initialize an LSI transformation
corpus_lsi = lsi[corpus_tfidf] # create a double wrapper over the original corpus: bow->tfidf->fold-in-lsi

print "*********************** Topics *********************"
print lsi.print_topics(2)
for doc in corpus_lsi: # both bow->tfidf and tfidf->lsi transformations are actually executed here, on the fly
    print(doc)

lsi.save('/tmp/model.lsi') # same for tfidf, lda, ...

lsi = models.LsiModel.load('/tmp/model.lsi')



exit(0)





#################### Choisir les candidats
Candidats = []
del laPlusPropre
frequences=[]
j=0
while j<len(laPlusPropreAvecFreq):

    if  (laPlusPropreAvecFreq[j+1] > 4) and (len(laPlusPropreAvecFreq[j])>3):
        frequences.append(laPlusPropreAvecFreq[j+1])
        Candidats.append(laPlusPropreAvecFreq[j])

    j+=2
########## ordonner les fréquences des candidats du plus fréquent au moins fréquent
s = [i[0] for i in sorted(enumerate(frequences), key=lambda x:x[1])]

########## reconstruire la liste des candidat
j=len(s)-1
Autre=[]
Last=[]
while (j>=0):
    pos = s[j]
    Last.append(Candidats[pos])
    Autre.append(Candidats[pos])
    Last.append(frequences[pos])
    j-=1

############## rendre les noms pluriels, singulier
for i in range(0, len(Autre)-1):
    matchPlural= re.search(r's$|x$',Autre[i])
    if matchPlural:
        single=Autre[i][:-1]
        if single in Autre:
            Autre[i]=single

uniquelist = []

######## suppression des mots redondants
[uniquelist.append(x) for x in Autre if x not in uniquelist]

############# NLTK POS-Tagger
from nltk.tag.stanford import POSTagger

french_postagger = POSTagger('models/french.tagger', 'stanford-postagger.jar')
print "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
phrase = ""
#print 'taille unique liste:', len(uniquelist)
for mot in uniquelist:
    phrase = phrase+' '+ mot
Result = french_postagger.tag(phrase.split())
#print Result
temporaire = []
#print Result[0]
for i in range (0, len(Result[0])-1):
    if (Result[0][i][1]==u'NC') or (Result[0][i][1]==u'N')or (Result[0][i][1]==u'ADJ'):
    ##if (Result[0][i][1]==u'VINF') or (Result[0][i][1]==u'ADV')or (Result[0][i][1]==u'VPP'):
        #print Result[0][i][0]
        #print Result[0][i][1]
        temporaire.append(Result[0][i][0])
Tab = []
for mot in uniquelist:
    if mot in temporaire:
        Tab.append(mot)

#print "taille Tab:", len(Tab)
#print Tab

del uniquelist
#uniquelist=[]
uniquelist = Tab





#print uniquelist
#######################
############# Stanford NER ###########
from nltk.tag.stanford import NERTagger
import elasticsearch
import random
st = NERTagger('/usr/share/classifiers/english.all.3class.distsim.crf.ser.gz','/usr/share/stanford-ner-3.5.1.jar')
indexe = 'test'
type = 'attachment'

es = elasticsearch.Elasticsearch(["127.0.0.1:9200"])

res = es.search(index=indexe, body=
{
    "fields": [
        "file"
    ]
}, size=1000)
random.seed(1)
sample = res['hits']['hits']
#print("Got %d Hits:" % res['hits']['total'])

for hit in sample:
    if hit["_id"]=="2":
        try:
            organisationTemp = []
            personTemp = []
            locationTemp = []
            col2 = hit["fields"]["file"][0]
            NER=st.tag(col2.split())

            for i in range(0, (len(NER[0])-1)):
                if(NER[0][i][1] == u'ORGANIZATION'):
                    organisationTemp.append(NER[0][i][0])
                if(NER[0][i][1] == u'LOCATION'):
                    locationTemp.append(NER[0][i][0])
                    print NER[0][i][0]
                if(NER[0][i][1] == u'PERSON'):
                    personTemp.append(NER[0][i][0])

        except Exception, e:
            print e
organisation=[]
location=[]
person=[]
[organisation.append(x) for x in organisationTemp if x not in organisation]
[location.append(x) for x in locationTemp if x not in location]
[person.append(x) for x in personTemp if x not in person]
print "organisations:", organisation
print "locations", location
print "persons",person

########## Calcul des correlations
import gensim

model = gensim.models.Word2Vec.load("wiki.fr.word2vec.model")

j=0
i=1
SimilairePremier = []
SimilaireDeuxieme =[]
SimilaireTroisieme = []

z = (len(uniquelist)-1)
toSend=[]
while j<z:
    try:
        premierMot= uniquelist[j]
        while i<(len(uniquelist)-1):
            try:

                deuxiemeMot=uniquelist[i]
                Taux= model.similarity(premierMot, deuxiemeMot)
                if (Taux < 1.0000000000000000) and (Taux >= 0.5000000000000000):
                    SimilairePremier.append(uniquelist[j])
                    SimilaireDeuxieme.append(uniquelist[i])
                    SimilaireTroisieme.append(Taux)
                    toSend.append(uniquelist[j])
                    toSend.append(uniquelist[i])
                #print "result:",Last[j] , Last[i], Taux
            except Exception, e:
                print e
                pass
            i+=1
        j+=1
        i = j+1
        #print j
    except Exception, e:
        print e
        pass


s = [i[0] for i in sorted(enumerate(SimilaireTroisieme), key=lambda x:x[1])]


j=len(s)-1

Ordered=[]
while (j>=0):
    pos = s[j]
    Ordered.append(SimilairePremier[pos])
    Ordered.append(SimilaireDeuxieme[pos])
    Ordered.append(SimilaireTroisieme[pos])
    j-=1

out_file = open("Ordred.json", "w")
Exemple = {}
Exemple['similarité_word2vec'] = Ordered
toTranslate=[]

for i in range(0, len(toSend)-1):
    matchPlural= re.search(r's$',toSend[i])
    if matchPlural:
        single=toSend[i][:-1]
        if single in toSend:
            print single
            print toSend[i]
            toSend[i]=single

del uniquelist
uniquelist = []

[uniquelist.append(x) for x in toSend if x not in uniquelist]

#print uniquelist

print "nombre des candidats", len(uniquelist)

finalList=[]
for i in range (0, 20):
    finalList.append(uniquelist[i])

print finalList

Exemple["candidats_Word2vec"]= finalList

json.dump(Exemple, out_file, indent=4)
out_file.close()