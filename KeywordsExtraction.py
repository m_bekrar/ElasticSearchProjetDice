__author__ = 'mbekrar'
import rake
import operator
import io

rake_object = rake.Rake("SmartStoplist.txt", 5, 3, 6)

#sample_file = open("data/docs/fao_test/France.txt", 'r')
#text = sample_file.read()
#keywords = rake_object.run(text)
#print "Keywords:", keywords

stoppath = "SmartStoplist.txt"

rake_object = rake.Rake(stoppath)

f = io.open("data/docs/fao_test/France.txt", 'r', encoding='utf8')

text = f.read().replace('\n', '')

f.close()

print text
#text = str(text)
#print type(text)
sentenceList = rake.split_sentences(text)
stopwordpattern = rake.build_stop_word_regex(stoppath)
phraseList = rake.generate_candidate_keywords(sentenceList, stopwordpattern)
wordscores = rake.calculate_word_scores(phraseList)
keywordcandidates = rake.generate_candidate_keyword_scores(phraseList, wordscores)
sortedKeywords = sorted(keywordcandidates.iteritems(), key=operator.itemgetter(1), reverse=True)
totalKeywords = len(sortedKeywords)

for keyword in sortedKeywords[0:(totalKeywords / 3)]:
    print "Keyword: ", keyword[0], ", score: ", keyword[1]
#keywords = rake_object.run(text)
#print "Keywords:", keywords

