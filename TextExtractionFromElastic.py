import elasticsearch
import csv
import random
import re
import json
import urllib2
import datetime
import requests


def extract_urls(your_text):
    url_re = re.compile(
        r'(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?\xab\xbb\u201c\u201d\u2018\u2019]))')
    for match in url_re.finditer(your_text):
        yield match.group(0)


now = datetime.datetime.now().date()
now = str(now)
# print now
es = elasticsearch.Elasticsearch(["127.0.0.1:9200"])

res = es.search(index="test", body=
{
    "fields": [
        "file"
    ]
}, size=500)
random.seed(1)
sample = res['hits']['hits']

print("Got %d Hits:" % res['hits']['total'])

out_file = open("Frenotliens.json", "w")

with open('pourjson.csv', 'wb') as csvfile:  # set name of output file here
    filewriter = csv.writer(csvfile, delimiter='\t', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    # create header row
    filewriter.writerow(["id", "title", "file", "liens"])  #change the column labels here
    for hit in sample:

        try:
            print '+++++++++++++ id +++++++++++\n'
            col1 = hit["_id"]
            print 'id= ' + col1

            reslien = es.get(index="test", doc_type='attachment', id=col1)
            #print(reslien['_source']['title'])
            col3 = reslien['_source']['title']
            col3 = str(col3)
            print '++++++++++++ titre ++++++++++++\n'
            print 'titre= ' + col3
        except Exception, e:
            print '++++++++++++ titre ++++++++++++\n'
            col3 = ""
            print 'titre=' + col3
        try:
            print '+++++++++++++++++++ liens ++++++++++++++++++++\n'
            col2 = hit["fields"]["file"]
            col2 = str(col2)
            uri = ''
            url = ''
            total = 0
            nbOK = 0
            simpleList = []
            for uri in extract_urls(col2):
                uri = uri.replace('\\n', '')
                uri = uri.replace('\\t', '')
                uri = uri.replace('\\r', '')
                uri = uri.replace('\\xad', '')
                uri = uri.replace('\\u2010', '')
                uri = uri.replace('nhttp', 'http')
                if uri.startswith('www.'):
                   uri = "http://" + uri

                if not uri.startswith('http'):
                        urii = "http://" + uri
                        ret = urllib2.urlopen(urii)
                        if ret.code == 200:
                            uri = urii
                        else:
                            uris = "https://" + uri
                            ret = urllib2.urlopen(uris)
                            if ret.code == 200:
                                uri = uris
                            else:
                                uri = "http://" + uri

                total += 1
                try:
                    ret = urllib2.urlopen(uri)
                    r = requests.get(uri, allow_redirects=False)

                    if ret.code == 200:
                        print uri
                        if r.status_code == 301:
                            stat = 'KO'
                            print "redirection!"
                        else:
                            print "Le site existe!"
                            stat = 'OK'
                            nbOK += 1


                except urllib2.HTTPError, e:
                    print uri
                    print(e.code)
                    stat = 'KO'
                except urllib2.URLError, e:
                    print uri
                    print(e.args)
                    stat = 'KO'

                lien = dict(url=uri, status=stat, lastchecked=now)
                x = lien
                simpleList.append(x)
                #lien = {u"url": uri, u"status": stat, u"lastchecked": now }


        except Exception, e:
            col2 = ""
        if total > 0:
            percent = "{0:.0f}%".format(float(nbOK) / total * 100)
        else:
            percent = "{0:.0f}%".format(float(0))

        sample = {}
        sample['id'] = col1
        sample['titre'] = col3
        sample['liens'] = simpleList
        sample['score'] = percent

        json.dump(sample, out_file, indent=4)
        #put (lien, "Frenotliens.json")
        filewriter.writerow([col1, col3, col2, url])
    print('Fini !')

out_file.close()