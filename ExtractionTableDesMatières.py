# coding=utf-8
import elasticsearch
import random
import io
import re
import json
from cStringIO import StringIO
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from pyPdf import PdfFileReader

############## Extraction des pages par PDFMiner################
def convert(fname, pages=None):
    if not pages:
        pagenums = set()
    else:
        pagenums = set(pages)

    output = StringIO()
    manager = PDFResourceManager()
    converter = TextConverter(manager, output, laparams=LAParams())
    interpreter = PDFPageInterpreter(manager, converter)

    infile = file(fname, 'rb')
    for page in PDFPage.get_pages(infile, pagenums):
        interpreter.process_page(page)
    infile.close()
    converter.close()
    text = output.getvalue()
    output.close
    return text


################## Remplacement des chaines de caractères#########################
def lreplace(pattern, sub, string):
    """
    remplace au début
    """
    return re.sub('^%s' % pattern, sub, string)


def rreplace(pattern, sub, string):
    """
    remplace à la fin
    """
    return re.sub('%s$' % pattern, sub, string)


########
def whatisthis(s):
    if isinstance(s, str):
        print "ordinary string"
    elif isinstance(s, unicode):
        print "unicode string"
    else:
        print "not a string"

#################### Les extraits ###########
def Extraction(tailleTitre, Titre, regex_1, regex_2, regex_3, pages, col3):
        j = 0
        trouve = False
        while (j < tailleTitre) and (not trouve):
            technique = re.search(regex_1, Titre[j], re.M | re.I)
            if technique:
                trouve = True
                title = technique.group()
                print Titre[j]
                pagetechnique = re.search(r'[0-9]+', Titre[j+1])
                pagetech = int(pagetechnique.group())

                if pagetechnique:
                    print " (premiere solution):", pagetech
                cleaning = re.search(regex_2, Titre[j], re.M | re.I)
                if cleaning:
                    clean = cleaning.group()
            j += 1

        if not trouve:
            j = 0
            while (j < tailleTitre) and (not trouve):
                techni = re.search(regex_3, Titre[j], re.M | re.I)
                if techni:
                    clean = techni.group()
                    trouve = True
                    pagetechni= re.search(r'[0-9]+', Titre[j+1])
                    if pagetechni:
                        pagetech = int(pagetechni.group())
                        print "(deuxieme solution):", pagetech
                j += 1

        i = pagetech - 2
        title = clean
        print title
        Found = False
        while (i < pages ) and (not Found):
            txtt = convert(col3, pages=[i, i])
            txtt = txtt.decode('utf-8')
            matchTitle = re.search(r'(\s*)' + title + r'(\s*)', txtt, re.M | re.I)
            print matchTitle
            if matchTitle:
                print "Fount in page: ",  i
                print txtt
                Found = True
                f = io.open(filename, 'w', encoding='utf8')
                f.write(txtt)
                f.close()
                f = io.open(filename, 'r', encoding='utf8')
                lignes = f.readlines()
                maxx = len(content)
                f.close()
                tit = False
                h = 0
                while (h < maxx) and (not tit):
                    nonextrait = re.search(r'(\s*)' + title + r'(\s*)', lignes[h], re.M | re.I)
                    if nonextrait:
                        tit = True
                        startExtraction = h
                        print "titre se trouve dans la ligne :", h
                    h += 1
                extrait = ''
                startExtraction = startExtraction + 1
                endExtraction = startExtraction + 7

                for d in range (startExtraction,endExtraction+6 ):
                    extrait = extrait+lignes[d]


            i += 1

        return extrait, pagetech

################### Récupération du texte extrait par elasticSearch #####################
es = elasticsearch.Elasticsearch(["127.0.0.1:9200"])

res = es.search(index="test", body=
{
    "fields": [
        "file"
    ]
}, size=500)
random.seed(1)
sample = res['hits']['hits']
out_file = open("Structure.json", "w")
print("Got %d Hits:" % res['hits']['total'])

for hit in sample:

    try:
        print '+++++++++++++ id +++++++++++\n'
        col1 = hit["_id"]
        print 'id= ' + col1

        reslien = es.get(index="test", doc_type='attachment', id=col1)
        col3 = reslien['_source']['title']
        col3 = str(col3)
        print '++++++++++++ titre ++++++++++++\n'
        print 'titre= ' + col3
    except Exception, e:
        print '++++++++++++ titre ++++++++++++\n'
        col3 = ""
        print 'titre=' + col3
    try:
        print '+++++++++++++++++++ liens ++++++++++++++++++++\n'
        col2 = hit["fields"]["file"][0]
        filename = "newfile.txt"

        ############## Ecriture et lecture du text extrait d'ElasticSearch dans le fichier ####################
        f = io.open(filename, 'w', encoding='utf8')
        f.write(col2)
        f.close()
        f = io.open(filename, 'r', encoding='utf8')
        content = f.readlines()
        max = len(content)
        print "taille du fichier:", max
        f.close()
        ##################### Detection de la premiere ligne de la table des matieres ####################
        Debut = 0
        i = 0
        trouves = False
        maxligne = int (max/4)
        while (i <= maxligne) and (not trouves):
            matchObj = re.search(r"(Table des mati.*|Sommaire)", content[i], re.M | re.I)
            if matchObj:
                print matchObj.group()
                Debut = i
                print "Debut de la table", Debut
                start = Debut + 1
                trouves = True
            i += 1


        ############################### Detection des lignes de la table des matieres et leurs ecritures dans une table#####################
        i = start
        Titre = []
        Table = []
        delete = False
        taille = -1
        tailleTitre = -1
        k = 0
        # print "i= ", i
        while (i <= maxligne) and (not delete):
            matchEntry = re.search(
                r'^([0-9]+\)*\s+|[0-9]+\.[0-9]*\)*\.*\s+|[a-zA-Z]\.\s+|[a-zA-Z]\)\s+|[a-zA-Z]\.[a-zA-Z]\)*\.\s+|[IXVLCDM]+\.\s+|[IXVLCDM]+\)\s+|[IXVLCDM]+\.[0-9]*\.*[a-zA-Z]*\.*\)*\s+|[a-zA-Z]\.[0-9]\)*\s+)*((.*)\s)+([.]*\s)+[0-9]+',
                content[i], re.M | re.I)
            if matchEntry:
                delete = re.search(r'^(FIGURE|TABLE)+\s+[0-9]', content[i], re.M | re.I)
                if not delete:
                    k += 1
                    num = re.search(
                        r'([0-9]+\)*\s+|[0-9]+\.[0-9]*\)*\.*\s+|[a-zA-Z]\.\s+|[a-zA-Z]\)\s+|[a-zA-Z]\.[a-zA-Z]\)*\.\s+|[IXVLCDM]+\.\s+|[IXVLCDM]+\)\s+|[IXVLCDM]+\.[0-9]*\.*[a-zA-Z]*\.*\)*\s+|[a-zA-Z]\.[0-9]\)*\s+)',
                        content[i], re.M | re.I)
                    if num:
                        numero = num.group()
                    else:
                        numero = ""

                    Table.append(numero)
                    chaine = content[i]
                    chaine.encode('utf-8')
                    chaine = lreplace(numero, "", chaine)
                    pag = re.search(r'\s[0-9]{1,2}\s', content[i])
                    if pag:
                        page = pag.group()
                        chaine = rreplace(page, "", chaine)
                        numpage = re.search(r'[0-9]+', page, re.M | re.I)
                        if numpage:
                            page = numpage.group()

                        chaine = chaine.replace('\\n', '')
                        page = "page:" + page
                    else:
                        page = ""
                    chaine = chaine.replace('.', "")
                    Table.append(chaine)
                    Table.append(page)
                    ################ Table des titres
                    print matchEntry.group()
                    titre = numero+chaine
                    Titre.append(titre)
                    Titre.append(page)
                    #print titre
                    taille += 3
                    tailleTitre += 2
                if delete:
                    fin = i
                    # deleted = True
                    print "Fin du Tableau", fin
            i += 1

        print 'Reconnaissance et Extraction de la table sont finis'
        print "nombre de ligne dans la table des matieres: ", k
        print Table
        print Titre

        #######################Extraction avec PDFMINER si ça existe dans le document#########
        count = 0
        sauvgarde = 0
        existe = False
        pdf = PdfFileReader(open(col3, 'rb'))
        pages = pdf.getNumPages()
        pagess = int(pages/4)
        print pagess
        for i in range(0, pagess):
            txt = convert(col3, pages=[i, i])
            # txt = str(txt)
            #whatisthis(txt)
            #print type(txt)
            txt = txt.decode('utf-8')
            match = re.search(r'\s*R\xe9sum\xe9 ex\xe9cutif|RESUME OPERATIONNEL', txt, re.M | re.I)
            if match:
                count += 1
                matchObj = re.search(r"(Table des mati.*|Sommaire)", txt, re.M | re.I)
                if matchObj:
                    count -= 1
                    print "Elle existe dans la table des matieres dans la page", i
                else:
                    sauvgarde = i
                    print "la vrai page :", i
                    existe = True
                    print txt
                    ChoixFinal = txt

        print 'count:', count

        if (count == 0) and (not existe):
            for i in range(0, pagess):
                txt = convert(col3, pages=[i, i])
                txt = txt.decode('utf-8')
                match = re.search(r'\s*R\xe9sum\xe9\s*|\s*Resume\s*|RESUME OPERATIONNEL', txt, re.M | re.I)
                if match:
                    count += 1
                    matchObj = re.search(r"(Table des mati.*|Sommaire)", txt, re.M | re.I)
                    if matchObj:
                        count -= 1
                        print "Elle existe dans la table des matieres dans la page", i
                    else:
                        sauvgarde = i
                        print "la vrai page :", i
                        print txt
                        ChoixFinal = txt

        ##################### Extraction #######################
        regex_1_technique = '(.*)\s*Analyse technique (.*)|(.*)\s*\xc9tude technique (.*)|(.*)\s*Etude technique (.*)'
        regex_2_technique = 'Analyse technique (\s*(.*))*|\xc9tude technique (\s*(.*))*|Etude technique (\s*(.*))*'
        regex_3_technique = '(\s*(.*))* technique (\s*(.*))*'
        extraitTechnique, pagetechnique = Extraction(taille, Table, regex_1_technique, regex_2_technique, regex_3_technique, pages, col3)
        regex_1_marche = '(.*)\s* \xc9tude de la concurrence (\s*(.*))*|(.*)\s* Etude de la concurrence (\s*(.*))*'
        regex_2_marche = '\xc9tude de la concurrence (\s*(.*))*|Etude de la concurrence'
        regex_3_marche = 'Etude du march\xe9|\xc9tude du march\xe9|Etude de march\xe9|\xc9tude de march\xe9'
        extraitMarche, pagemarche = Extraction(taille, Table, regex_1_marche, regex_2_marche, regex_3_marche, pages, col3)
        regex_1_social = '(.*)\s* \xc9tude sociologique (\s*(.*))*|(.*)\s* Etude sociologique (\s*(.*))*'
        regex_2_social = '\xc9tude sociologique (\s*(.*))*|Etude sociologique'
        regex_3_social = '(.*) innovation (\s*(.*))*|(.*) innova(.*) (\s*(.*))*|(.*) notre produit (\s*(.*))*|(.*) notre offre (\s*(.*))*'
        extraitSocial, pagesocial = Extraction(taille, Table, regex_1_social, regex_2_social, regex_3_social, pages, col3)
        #####################################
        pagesocialfin = pagemarche - 1
        pagemarchefin = pagetechnique -1
        #pagetechniquefin = pages - 1

        sample = {}
        sample['_id'] = col1
        sample['Titre'] = col3
        sample['Note'] = ""
        sample['Abstract'] = ChoixFinal
        sample['Structure'] = Titre
        del Titre
        del Table
        #
        sample['Extracts'] = {}
        sample['Extracts']['marche']= {}
        sample['Extracts']['marche']['page'] = str(pagemarche)+"-"+str(pagemarchefin)
        sample['Extracts']['marche']['abstract'] = extraitMarche
        sample['Extracts']['technique'] = {}
        sample['Extracts']['technique']['page'] = str(pagetechnique)
        sample['Extracts']['technique']['abstract']= extraitTechnique
        sample['Extracts']['Social'] = {}
        sample['Extracts']['Social']['page'] = str(pagesocial)+"-"+ str(pagesocialfin)
        sample['Extracts']['Social']['abstract'] = extraitSocial
        #json.dumps(sample, ensure_ascii=False).encode('utf8')
        json.dump(sample, out_file, indent=4)

    except Exception, e:
        print e
out_file.close()
